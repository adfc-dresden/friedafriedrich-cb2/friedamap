<?php
/*
Plugin Name: Friedamap
Description: Frieda & Friedrich style map for Commonsbooking2. Use shortcode [friedamap]
Author: Nils Larsen
*/

function enqueue_leaflet_assets() {
    wp_register_style('leaflet-style', 'https://unpkg.com/leaflet@1.9.4/dist/leaflet.css');
    wp_register_script('leaflet-script', 'https://unpkg.com/leaflet@1.9.4/dist/leaflet.js');
    wp_register_script('friedamap-script', plugin_dir_url(__FILE__) . 'friedamap.js', array('leaflet-script'), null, true);

    wp_localize_script( 'friedamap-script', 'friedamap_vars', array( 'pluginUrl' => plugin_dir_url(__FILE__) ) );
}
add_action('wp_enqueue_scripts', 'enqueue_leaflet_assets');

function friedamap_shortcode($atts) {
    wp_enqueue_style('leaflet-style');
    wp_enqueue_script('friedamap-script');

    //default values:
    $atts = shortcode_atts(array(
        'width' => '100%',
        'height' => '500px',
        'lat' => 50.9021,
        'lon' => 14.8133,
        'zoom' => 14
    ), $atts);

    $map_html = <<<EOD
    <p>
    <label><input type="checkbox" id="zweiradCheckbox">Zweirad</label> 
    <label><input type="checkbox" id="dreiradCheckbox">Dreirad</label> 
    <label><input type="checkbox" id="rikschaCheckbox">Rikscha</label> 
    <label><input type="checkbox" id="trailerCheckbox">Anhänger</label> | 
    <label><input type="checkbox" id="todayCheckbox">Heute verfügbar</label> |
    <label><input type="checkbox" id="mitMotorCheckbox">Motor</label> |
    <label><input type="checkbox" id="mitKindersitzCheckbox">Kindersitz</label>
    </p>
    EOD;

    $map_html .= '<div id="map" style="width:' . esc_attr($atts['width']) . '; aspect-ratio: 1/1; max-height:' . esc_attr($atts['height']) . ';"></div>';

    wp_localize_script( 'friedamap-script', 'friedamap_center', array( 'lat' => $atts['lat'], 'lon' => $atts['lon'], 'zoom' => $atts['zoom'] ) );

    return $map_html;
}
add_shortcode('friedamap', 'friedamap_shortcode');
