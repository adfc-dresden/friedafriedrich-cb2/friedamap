const locationsEndpoint =    '/wp-json/commonsbooking/v1/locations';
const itemsEndpoint =        '/wp-json/wp/v2/cb_item';
const categoriesEndpoint =   '/wp-json/wp/v2/cb_items_category';
const availabilityEndpoint = '/wp-json/availabilitycache/v1/availability';

const baseUrlForItemSlug =   '/artikel/';

let jsTemplateDir = '';

// when part of Wordpress plugin, get plugin URL via script localization
if (typeof friedamap_vars !== 'undefined') {
	jsTemplateDir = friedamap_vars['pluginUrl'];
}

let friedaMapCenter = [50.9021, 14.8133];
let friedaMapZoom = 14;

// when part of Wordpress plugin, get center coordinates and zoom via script localization
if (typeof friedamap_center !== 'undefined') {
	friedaMapCenter = [friedamap_center['lat'], friedamap_center['lon']];
	friedaMapZoom = friedamap_center['zoom'];
}

const generateIcon = (url) => L.icon({
        iconUrl: url,
        shadowUrl: jsTemplateDir + 'icon-shadow2.png',
        iconSize: [29, 43], // size of the icon
        shadowSize: [50, 58], // size of the shadow
        iconAnchor: [14.5, 43], // point of the icon which will correspond to marker's location
        shadowAnchor: [19, 47], // the same for the shadow
        popupAnchor: [0, -43] // point from which the popup should open relative to the iconAnchor
    });

const ItemType = {
	TwoWheel: 2,
	ThreeWheel: 3,
	Rickshaw: 4,
	Trailer: 5,
};

const brands = {
	'Christiania': {icon: generateIcon(jsTemplateDir + 'christiania-2.png'), itemType: ItemType.ThreeWheel },
	'Bullitt': {icon: generateIcon(jsTemplateDir + 'bullitt-2.png'), itemType: ItemType.TwoWheel },
	'Hilde': {icon: generateIcon(jsTemplateDir + 'hilde.png'), itemType: ItemType.Trailer },
	'Rikscha': {icon: generateIcon(jsTemplateDir + 'rikscha-2.png'), itemType: ItemType.Rickshaw },
	'Musketier': {icon: generateIcon(jsTemplateDir + 'musketier.png'), itemType: ItemType.ThreeWheel },
	'Omnium': {icon: generateIcon(jsTemplateDir + 'omnium.png'), itemType: ItemType.TwoWheel },
	'viele': {icon: generateIcon(jsTemplateDir + 'viele.png'), itemType: null },
};

// returns a html-formatted <ul> list of items (Frieda, Friedrich, [...] und Heidelore)
const generateItemList = (items) => {
    let itemList = '';
    for (let i = 0; i < items.length; i++) {
        x = items[i];
        itemList += `<li><a href="${baseUrlForItemSlug}${x.itemSlug}">${x.itemName} (jetzt buchen!)</a>`;
        if (items.length > 1 && i == items.length - 2) {
            itemList += " und ";
        } else if (items.length > 2 && i <= items.length - 2) {
            itemList += ", ";
        }
        itemList += "</li>";
    }
    return "<ul>" + itemList + "</ul>";
};

function filterAvailableEntries(dataAvailability) {
	const filteredAvailableEntries = {};
	const seenCombinations = new Set();
	dataAvailability.forEach(entry => {
		const combination = `${entry.locationId}-${entry.itemId}`;
		if (!seenCombinations.has(combination)) {
			seenCombinations.add(combination);
			if (!filteredAvailableEntries[entry.locationId]) {
				filteredAvailableEntries[entry.locationId] = [];
			}
			filteredAvailableEntries[entry.locationId].push(entry);
		}
	});

	return filteredAvailableEntries;
}

class MapManager {
	constructor() {
		if (MapManager.instance) return MapManager.instance; // Singleton pattern
		MapManager.instance = this;

		this.map = null;
		this.locations = [];
		this.brandsByCat = [];
		this.motorCatId = null;
		this.childTransportCatId = null;

		this.filterChangeCallback = this.filterChangeCallback.bind(this);
	}

	initMap() {
		this.map = L.map('map').setView(friedaMapCenter, friedaMapZoom);

		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			maxZoom: 19,
			attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(this.map);

		Promise.all([
			fetch(locationsEndpoint).then(response => response.json()),
			fetch(availabilityEndpoint).then(response => response.json()),
			fetch(itemsEndpoint + '?per_page=100&_fields=id,title,cb_items_category,slug').then(response => response.json()),
			fetch(categoriesEndpoint + '?per_page=100').then(response => response.json())
		]).then(([locationsData, availabilityData, itemsData, categoriesData]) => {
			this.processData(locationsData.locations, availabilityData.availability, itemsData, categoriesData);
		}).catch(error => {
			console.error('Error fetching data:', error);
		});

		// Hide rows in cb-items-table that are not currently in map view
		this.map.on('moveend', () => this.filterTableByMapView());
	}

	processData(dataLocations, dataAvailability, dataItems, dataCategories) {
		if (dataItems.length == 100 || dataCategories.length == 100) {
			console.warn("Items or Categories hit 100 and script does not support pagination...");
		}

		// Prepare array of icons by category id
		dataCategories.forEach(category => {
			if (brands[category.name]) {
				this.brandsByCat[category.id] = brands[category.name];
			}
			if (category.name == 'Motor') {
				this.motorCatId = category.id;
			} else if (category.name == 'Kindersitz') {
				this.childTransportCatId = category.id;
			}
		});

		// iterate through available-entries and get the first.-in-time available-entry that
		// is unique with respect to location and item
		let filteredAvailableEntries = filterAvailableEntries(dataAvailability);

		// iterate again to add item name and slug to each availability entry
		for (const locationId in filteredAvailableEntries) {
			filteredAvailableEntries[locationId].forEach(availability => {
				const itemId = parseInt(availability.itemId);
				const matchedItem = dataItems.find(entry => entry.id === itemId);

				availability.itemName = matchedItem.title.rendered;
				availability.itemName = availability.itemName.replace(/&#038;/g, '&'); // TODO cheap and dirty HTML-decode
				availability.itemSlug = matchedItem.slug;
				availability.itemCategories = matchedItem.cb_items_category;
			});
		}

		dataLocations.features.forEach(feature => {
			let loc = {};
			loc.lat = feature.geometry.coordinates[1];
			loc.lon = feature.geometry.coordinates[0];
			loc.locationName = feature.properties.name;
			let locationId = feature.properties.id;
			loc.availableEntries = filteredAvailableEntries[locationId];

			if (!loc.availableEntries) return;

			this.locations.push(loc);
		});

		this.locations.forEach(loc => {
			this.putMarkerOnMap(loc, {});
		});

		this.filterTableByMapView(); // Hide rows in cb-items-table that are not currently in map view
		document.getElementById('todayCheckbox').addEventListener('change', this.filterChangeCallback);
		document.getElementById('mitMotorCheckbox').addEventListener('change', this.filterChangeCallback);
		document.getElementById('zweiradCheckbox').addEventListener('change', this.filterChangeCallback);
		document.getElementById('dreiradCheckbox').addEventListener('change', this.filterChangeCallback);
		document.getElementById('rikschaCheckbox').addEventListener('change', this.filterChangeCallback);
		document.getElementById('trailerCheckbox').addEventListener('change', this.filterChangeCallback);
		document.getElementById('mitKindersitzCheckbox').addEventListener('change', this.filterChangeCallback);
	}

	filterChangeCallback(event) {
		let filters = {
			onlyToday: document.getElementById('todayCheckbox').checked,
			onlyMotor: document.getElementById('mitMotorCheckbox').checked,
			onlyTwoWheels: document.getElementById('zweiradCheckbox').checked,
			onlyThreeWheels: document.getElementById('dreiradCheckbox').checked,
			onlyRickshaw: document.getElementById('rikschaCheckbox').checked,
			onlyTrailer: document.getElementById('trailerCheckbox').checked,
			onlyChildTransport: document.getElementById('mitKindersitzCheckbox').checked,
		};

		this.locations.forEach(loc => {
			this.putMarkerOnMap(loc, filters);
		});

		this.filterTableByMapView();
	}

	putMarkerOnMap(loc, filters) {

		let opts = {
			IndexOffset: 1000
		};

		for (let ae of loc.availableEntries) {
			for (let catId of ae.itemCategories) {
				if (this.brandsByCat[catId]) {
					ae.icon = this.brandsByCat[catId].icon;
					ae.itemType = this.brandsByCat[catId].itemType;
				} else if (this.motorCatId && catId == this.motorCatId) {
					ae.motor = true;
				} else if (this.childTransportCatId && catId == this.childTransportCatId) {
					ae.childTransport = true;
				}
			}
		}

		loc.userFilteredAvailableEntries = [];

		for (let ae of loc.availableEntries) {
			if (filters.onlyMotor && (!ae.motor)) continue;
			if (filters.onlyTwoWheels || filters.onlyThreeWheels || filters.onlyRickshaw || filters.onlyTrailer) {
				if (filters.onlyTwoWheels == false && ae.itemType == ItemType.TwoWheel) continue;
				if (filters.onlyThreeWheels == false && ae.itemType == ItemType.ThreeWheel) continue;
				if (filters.onlyRickshaw == false && ae.itemType == ItemType.Rickshaw) continue;
				if (filters.onlyTrailer == false && ae.itemType == ItemType.Trailer) continue;
			}
			if (filters.onlyChildTransport && (!ae.childTransport)) continue;
			if (filters.onlyToday) {
				let todayUTCDate = new Date().toISOString().split("T")[0];
				if (todayUTCDate < ae.start.split("T")[0] || todayUTCDate > ae.end.split("T")[0]) {
					continue;
				}
			}
			loc.userFilteredAvailableEntries.push(ae);
		}

		let markerText = `<strong>${loc.locationName}</strong><br>Von hier `;
		markerText += loc.userFilteredAvailableEntries.length > 1 ? 'können ' : 'kann ';
		markerText += generateItemList(loc.userFilteredAvailableEntries);
		markerText += " ausgeliehen werden.";

		if (loc.userFilteredAvailableEntries.length > 1) {
			opts.icon = brands['viele'].icon;
		}
		else if (loc.userFilteredAvailableEntries.length == 1) {
			// icon may not be set (for example if no matching category is found),
			// so allow fallback with default marker by not setting opts.icon
			if ("icon" in loc.userFilteredAvailableEntries[0]) {
				opts.icon = loc.userFilteredAvailableEntries[0].icon;
			}
		}

		if (loc.marker && this.map.hasLayer(loc.marker)) {
			this.map.removeLayer(loc.marker);
		}

		if (loc.userFilteredAvailableEntries.length > 0) {
			loc.marker = L.marker([loc.lat, loc.lon], opts).addTo(this.map);
			loc.marker.bindPopup(markerText);
		}
	}
	
	// To hide rows in cb-items-table that are not currently in map view
	filterTableByMapView() {
		const bounds = this.map.getBounds();
		const visibleItems = [];

		this.locations.forEach((loc) => {
			if (loc.marker && bounds.contains(loc.marker.getLatLng())) {
				for (let ae of loc.userFilteredAvailableEntries) {
					visibleItems.push(ae.itemName);
				}
			}
		});

		// Filter the table rows based on the visible items
		const table = document.getElementsByClassName('cb-items-table')[0];
		if (!table) return;
		const rows = table.getElementsByTagName('tr');

		Array.from(rows).forEach((row) => {
			const titleDiv = row.querySelector('td a');
			if (titleDiv) {
				const itemName = titleDiv.textContent.trim();
				// Show or hide the row based on whether the item is visible on the map
				if (visibleItems.includes(itemName)) {
					row.style.display = ''; // Show
				} else {
					row.style.display = 'none'; // Hide
				}
			}
		});
	}
}

// Call the initMap function when the page loads
window.onload = () => {
	const mapManager = new MapManager();
	mapManager.initMap();
};

