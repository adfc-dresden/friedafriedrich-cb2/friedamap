<?php
/*
 * Plugin Name: Friedamap - cache for commonsbooking v2 availability output
 * Description: The cache is invalidated at midnight and when timeframes are changed in the backend. The plugin also discards availability-entries that are redundant with respect to map display, but the consuming javascript should not rely on filtering (it should stay compatible with original availability route of commonsbooking. URL: /wp-json/availabilitycache/v1/availability
 * Author: Nils Larsen
 */
 
function endpointUrlToCache() {
    return rest_url('commonsbooking/v1/availability');
}
 
// Hook into save_post, delete_post, and trashed_post actions for cb_timeframe post type
add_action('save_post_cb_timeframe', 'invalidate_api_cache');
add_action('delete_post_cb_timeframe', 'invalidate_api_cache');
add_action('trashed_post_cb_timeframe', 'invalidate_api_cache');

function invalidate_api_cache($post_id) {
    // Trigger asynchronous regeneration of the cache via a fire-and-forget REST call.
    // This avoids delaying the backend or frontend user experience.
    // Note: The updated cache will take a few seconds to be available.
    $url = rest_url( 'availabilitycache/v1/regenerate' );

    $response = wp_remote_post( $url, [
        'blocking' => false, // Don't wait for the response
    ] );

    if ( is_wp_error( $response ) ) {
        error_log( 'Failed to trigger availabilitycache regeneration: ' . $response->get_error_message() );
    }
}

function frieda_availabilitycache_regenerate() {
    $new_output = generate_api_response();
    if ($new_output) {
        set_transient('api_output', $new_output);
    }
    return $new_output;
}

// Function to get cached API response
function get_cached_api_response() {
    // Check if cache is present
    $cached_output = get_transient('api_output');

    // If cache is found, return cached output
    if ($cached_output !== false) {
        return $cached_output;
    }

    // Otherwise, generate new response, update cache, and return
    return frieda_availabilitycache_regenerate();
}

// iterate through available-entries and get the first available-entry that
// is unique with respect to location and item
function prefilterData($data) {
    $filteredData = array();
    $seenCombinations = array();

    foreach ($data['availability'] as $entry) {
        $combination = $entry['locationId'] . '-' . $entry['itemId'];
        
        if (!in_array($combination, $seenCombinations)) {
            $seenCombinations[] = $combination;
            $filteredData['availability'][] = $entry;
        }
    }
    return $filteredData;
}

function generate_api_response() {
    $response = wp_remote_get(endpointUrlToCache(), ['timeout' => 300]);

    if (is_array($response) && !empty($response['body'])) {
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body, true);

        // pre-filter data: remove availability-entries that will be discarded
        // in consuming javascript anyway to save bandwidth.
        // the consuming javascript should render same map no matter if the
        // data was pre-filered or not
        return prefilterData($data);
    }
    return false;
}

// Function to schedule daily cache invalidation
function schedule_daily_cache_invalidation() {
    if (!wp_next_scheduled('daily_cache_invalidation')) {
        wp_schedule_event(strtotime('midnight'), 'daily', 'daily_cache_invalidation');
    }
}
add_action('wp', 'schedule_daily_cache_invalidation');

// Function to invalidate the cache on a daily basis
function daily_cache_invalidation() {
    delete_transient('api_output');
}
add_action('daily_cache_invalidation', 'daily_cache_invalidation');

// Register custom REST API routes
function register_cached_api_endpoint() {
    // Register route to fetch cached availability
    register_rest_route(
        'availabilitycache/v1',
        '/availability', // Endpoint route
        array(
            'methods' => 'GET',
            'callback' => 'get_cached_api_response', // Callback to fetch the cached response
            'permission_callback' =>  '__return_true'
        )
    );

    // Register route to regenerate the cache
    register_rest_route(
        'availabilitycache/v1',
        '/regenerate',
        array(
            'methods'  => 'POST',
            'callback' => 'frieda_availabilitycache_regenerate',
            'permission_callback' => '__return_true',
        )
    );
}
add_action('rest_api_init', 'register_cached_api_endpoint');


?>